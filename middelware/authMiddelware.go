package middelware

import (
	"encoding/json"
	"net/http"

	"bitbucket.com/abhinavmishra094/ab_do/utils"
)

func AutherizeJWT(handler http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		const BEARER_SCHEMA = "Bearer "
		authHeader := r.Header.Get("Authorization")
		if authHeader == "" {
			respondWithError(w, http.StatusUnauthorized, "Authorization header is required")

		} else {

			tokenString := authHeader[len(BEARER_SCHEMA):]

			token, err := utils.NewJWTService().ValidateToken(tokenString)

			if token.Valid {
				// claims := token.Claims.(jwt.MapClaims)
				// log.Println("Claims[Name]: ", claims["name"])
				// log.Println("Claims[Admin]: ", claims["admin"])
				// log.Println("Claims[Issuer]: ", claims["iss"])
				// log.Println("Claims[IssuedAt]: ", claims["iat"])
				// log.Println("Claims[ExpiresAt]: ", claims["exp"])
				handler.ServeHTTP(w, r)
				return
			}
			json.NewEncoder(w).Encode(err)
		}
	}
}

func respondWithError(w http.ResponseWriter, code int, message string) {
	respondWithJSON(w, code, map[string]string{"error": message})
}

func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}
