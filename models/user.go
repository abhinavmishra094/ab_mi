package models

import (
	"database/sql"
	"fmt"
)

type User struct {
	ID       int    `json:"id"`
	Name     string `json:"name" validate:"required"`
	Email    string `json:"email" validate:"required,email"`
	Password string `json:"password" validate:"required,max=50,min=8"`
}

func (u *User) CreateUser(db *sql.DB) (int, error) {
	fmt.Println(u.Email)
	createUser := fmt.Sprintf("INSERT INTO users(name, email, password) VALUES('%s', '%s', '%s')", u.Name, u.Email, u.Password)
	res, err := db.Exec(createUser)
	if err != nil {
		return 0, err
	}
	id, err := res.LastInsertId()
	if err != nil {
		return 0, err
	}
	fmt.Println(err)
	if err != nil {
		return 0, err
	}
	return int(id), nil
}

func (u *User) GetUser(db *sql.DB) (*User, error) {
	var user User
	getUser := fmt.Sprintf("SELECT * FROM users WHERE email='%s'", u.Email)
	err := db.QueryRow(getUser).Scan(&user.ID, &user.Name, &user.Email, &user.Password)
	if err != nil {
		return nil, err
	}
	if user.ID == 0 {
		return nil, nil
	}
	return &user, nil
}

func (u *User) UpdateUser(db *sql.DB) error {
	updateUser := fmt.Sprintf("UPDATE users SET name='%s', email='%s', password='%s' WHERE id=%d", u.Name, u.Email, u.Password, u.ID)
	_, err := db.Exec(updateUser)
	if err != nil {
		return err
	}
	return nil
}
