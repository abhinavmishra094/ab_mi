package app

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"strconv"

	"bitbucket.com/abhinavmishra094/ab_do/middelware"
	"bitbucket.com/abhinavmishra094/ab_do/models"
	"bitbucket.com/abhinavmishra094/ab_do/utils"
	"github.com/go-playground/validator/v10"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"github.com/markbates/goth"
	"github.com/markbates/goth/gothic"
	"github.com/markbates/goth/providers/google"
)

type App struct {
	Router *mux.Router
	DB     *sql.DB
}

var (
	// key must be 16, 24 or 32 bytes long (AES-128, AES-192 or AES-256)
	key   = []byte("SESSION_KEY")
	store = sessions.NewCookieStore(key)
)

func (a *App) Initialize(user, password, dbname, hostname string) {
	var err error

	dataSourceName := fmt.Sprintf("%s:%s@tcp(%s)/%s", user, password, hostname, dbname)
	a.DB, err = sql.Open("mysql", dataSourceName)
	if err != nil {
		panic(err)
	}
	gothic.Store = store

	goth.UseProviders(
		google.New(os.Getenv("GOOGLECLIENTID"), os.Getenv("GOOGLECLIENTSECRET"), os.Getenv("GOOGLECALLBACKURL"), "email", "profile"),
	)

	a.Router = mux.NewRouter()
	a.Router.Use(mux.CORSMethodMiddleware(a.Router))
	a.initializeRoutes()

}

func respondWithError(w http.ResponseWriter, code int, message string) {
	respondWithJSON(w, code, map[string]string{"error": message})
}

func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}
func (a *App) Run(addr string) {
	// Listen and serve on
	fmt.Println("Listening on port " + addr)
	log.Fatal(http.ListenAndServe(":"+addr, a.Router))
}
func (a *App) signup(w http.ResponseWriter, r *http.Request) {
	var user models.User
	var err error
	validate := validator.New()
	decoder := json.NewDecoder(r.Body)
	if err = decoder.Decode(&user); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}
	err = validate.Struct(user)
	if err != nil {
		validationErrors := err.(validator.ValidationErrors)
		respondWithError(w, http.StatusBadRequest, validationErrors.Error())
	} else {
		defer r.Body.Close()
		user.Password = utils.HashPassword(user.Password)
		_, err = user.CreateUser(a.DB)
		if err != nil {
			respondWithError(w, http.StatusInternalServerError, err.Error())
		} else {
			jwtService := utils.NewJWTService()
			token, err := jwtService.GenerateToken(user.Name, false)
			if err != nil {
				respondWithError(w, http.StatusInternalServerError, err.Error())
			} else {
				respondWithJSON(w, http.StatusOK, map[string]string{"token": token})
			}
		}
	}

}
func (a *App) login(w http.ResponseWriter, r *http.Request) {
	jwtService := utils.NewJWTService()
	var user models.User
	var err error
	decoder := json.NewDecoder(r.Body)
	if err = decoder.Decode(&user); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
	}
	u, err := user.GetUser(a.DB)
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
	} else if u == nil {
		respondWithError(w, http.StatusInternalServerError, "Invalid user")
	} else {
		defer r.Body.Close()
		if utils.DoPasswordsMatch(u.Password, user.Password) {
			token, err := jwtService.GenerateToken(u.Name, false)
			if err != nil {
				respondWithError(w, http.StatusInternalServerError, err.Error())
			} else {
				respondWithJSON(w, http.StatusOK, map[string]string{"token": token})
			}

		} else {
			respondWithError(w, http.StatusUnauthorized, "Unauthorized")
		}
	}
}
func (a *App) getUser(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	email := vars["email"]
	user := models.User{Email: email}
	u, err := user.GetUser(a.DB)
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
	}
	u.Password = ""
	respondWithJSON(w, http.StatusOK, u)
}
func (a *App) updateUser(w http.ResponseWriter, r *http.Request) {
	var user models.User
	var err error
	validate := validator.New()
	decoder := json.NewDecoder(r.Body)
	if err = decoder.Decode(&user); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}
	if user.ID <= 0 {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	} else {
		err = validate.Struct(user)
		if err != nil {
			validationErrors := err.(validator.ValidationErrors)
			respondWithError(w, http.StatusBadRequest, validationErrors.Error())
		} else {
			defer r.Body.Close()
			user.Password = utils.HashPassword(user.Password)
			err = user.UpdateUser(a.DB)
			if err != nil {
				respondWithError(w, http.StatusInternalServerError, err.Error())
			} else {
				respondWithJSON(w, http.StatusOK, map[string]string{"result": "success"})
			}
		}
	}
}
func (a *App) index(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "ab_do")
	log.Println(session.Values["isAuth"])
	if session.Values["isAuth"] == true {
		t := template.Must(template.New("home.html").ParseGlob("templates/*.html"))
		if err := t.Execute(w, r.URL.Query()); err != nil {
			http.Error(w, fmt.Sprintf("error executing template (%s)", err), http.StatusInternalServerError)
		}
	} else {

		t := template.Must(template.New("index.html").ParseGlob("templates/*.html"))
		if err := t.Execute(w, r.URL.Query()); err != nil {
			http.Error(w, fmt.Sprintf("error executing template (%s)", err), http.StatusInternalServerError)
		}
	}
}
func (a *App) loginHtml(w http.ResponseWriter, r *http.Request) {
	t := template.Must(template.New("login.html").ParseGlob("templates/*.html"))
	if err := t.Execute(w, r.URL.Query()); err != nil {
		http.Error(w, fmt.Sprintf("error executing template (%s)", err), http.StatusInternalServerError)
	}
}
func (a *App) register(w http.ResponseWriter, r *http.Request) {
	t := template.Must(template.New("register.html").ParseGlob("templates/*.html"))
	if err := t.Execute(w, r.URL.Query()); err != nil {
		http.Error(w, fmt.Sprintf("error executing template (%s)", err), http.StatusInternalServerError)
	}
}
func (a *App) home(w http.ResponseWriter, r *http.Request) {
	// log.Println("home")

	t := template.Must(template.New("home.html").ParseGlob("templates/*.html"))
	if err := t.Execute(w, r.URL.Query()); err != nil {
		http.Error(w, fmt.Sprintf("error executing template (%s)", err), http.StatusInternalServerError)
	}
}
func (a *App) loginForm(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "ab_do")
	jwtService := utils.NewJWTService()
	email := r.FormValue("email")
	password := r.FormValue("password")
	// log.Println(password)
	var user = models.User{
		Email:    email,
		Password: password,
	}
	var err error
	u, err := user.GetUser(a.DB)
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
	} else if u == nil {
		respondWithError(w, http.StatusInternalServerError, "Invalid user")
	} else {
		defer r.Body.Close()
		if utils.DoPasswordsMatch(u.Password, user.Password) {
			token, err := jwtService.GenerateToken(u.Name, false)
			if err != nil {
				respondWithError(w, http.StatusInternalServerError, err.Error())
			} else {
				session.Values["id"] = user.ID
				session.Values["name"] = user.Name
				session.Values["email"] = user.Email
				session.Values["isAuth"] = true
				session.Values["token"] = token
				session.Save(r, w)
				t := template.Must(template.New("home.html").ParseGlob("templates/*.html"))
				if err := t.Execute(w, r.URL.Query()); err != nil {
					http.Error(w, fmt.Sprintf("error executing template (%s)", err), http.StatusInternalServerError)
				}
			}

		} else {
			respondWithError(w, http.StatusUnauthorized, "Unauthorized")
		}
	}
}
func (a *App) editForm(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "ab_do")
	if session.Values["isAuth"] == true {
		user := models.User{
			ID:    session.Values["id"].(int),
			Name:  session.Values["name"].(string),
			Email: session.Values["email"].(string),
		}

		if user.Name == "" {
			u, _ := user.GetUser(a.DB)
			user.Name = u.Name
		}
		// log.Println("#240", user)
		t := template.Must(template.New("profile.html").ParseGlob("templates/*.html"))
		if err := t.Execute(w, user); err != nil {
			http.Error(w, fmt.Sprintf("error executing template (%s)", err), http.StatusInternalServerError)
		}
	} else {
		t := template.Must(template.New("index.html").ParseGlob("templates/*.html"))
		if err := t.Execute(w, r.URL.Query()); err != nil {
			http.Error(w, fmt.Sprintf("error executing template (%s)", err), http.StatusInternalServerError)
		}
	}
}
func (a *App) registerForm(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "ab_do")
	name := r.FormValue("username")
	email := r.FormValue("email")
	password := r.FormValue("password")

	var user = models.User{
		Name:     name,
		Email:    email,
		Password: utils.HashPassword(password),
	}
	id, err := user.CreateUser(a.DB)
	user.ID = id
	user.Password = password
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
	} else {
		jwtService := utils.NewJWTService()
		token, err := jwtService.GenerateToken(user.Name, false)
		session.Values["id"] = user.ID
		session.Values["name"] = name
		session.Values["email"] = email
		session.Values["isAuth"] = true
		session.Values["token"] = token
		session.Save(r, w)
		if err != nil {
			respondWithError(w, http.StatusInternalServerError, err.Error())
		}
		t := template.Must(template.New("profile.html").ParseGlob("templates/*.html"))
		if err := t.Execute(w, user); err != nil {
			http.Error(w, fmt.Sprintf("error executing template (%s)", err), http.StatusInternalServerError)
		}
	}
}
func (a *App) updateForm(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(r.FormValue("id"))
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	name := r.FormValue("username")
	email := r.FormValue("email")
	password := r.FormValue("password")
	// fmt.Println(name, email, password)
	var user = models.User{
		ID:       id,
		Name:     name,
		Email:    email,
		Password: utils.HashPassword(password),
	}
	// fmt.Println(user)
	err = user.UpdateUser(a.DB)
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
	}
	jwtService := utils.NewJWTService()
	token, err := jwtService.GenerateToken(user.Name, false)
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	w.Header().Set("Authorization", token)
	// log.Println("home")
	t := template.Must(template.New("home.html").ParseGlob("templates/*.html"))
	if err := t.Execute(w, user); err != nil {
		http.Error(w, fmt.Sprintf("error executing template (%s)", err), http.StatusInternalServerError)
	}
}
func (a *App) logout(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "ab_do")
	session.Values["isAuth"] = false
	session.Save(r, w)
	t := template.Must(template.New("index.html").ParseGlob("templates/*.html"))
	if err := t.Execute(w, r.URL.Query()); err != nil {
		http.Error(w, fmt.Sprintf("error executing template (%s)", err), http.StatusInternalServerError)
	}
}

func (a *App) authGoogle(res http.ResponseWriter, req *http.Request) {
	gothic.BeginAuthHandler(res, req)
}
func (a *App) googleCallback(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "ab_do")
	user, err := gothic.CompleteUserAuth(w, r)
	log.Println(user)
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	u := models.User{
		Name:     user.Name,
		Email:    user.Email,
		Password: "",
	}
	id, err := u.CreateUser(a.DB)
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
	}
	u.ID = id
	jwtService := utils.NewJWTService()
	token, _ := jwtService.GenerateToken(u.Name, false)
	session.Values["id"] = u.ID
	session.Values["name"] = u.Name
	session.Values["email"] = u.Email
	session.Values["isAuth"] = true
	session.Values["token"] = token
	session.Save(r, w)
	t := template.Must(template.New("profile.html").ParseGlob("templates/*.html"))
	if err := t.Execute(w, u); err != nil {
		http.Error(w, fmt.Sprintf("error executing template (%s)", err), http.StatusInternalServerError)
	}
}
func (a *App) initializeRoutes() {

	a.Router.HandleFunc("/", a.index)
	a.Router.HandleFunc("/index", a.index).Methods("GET")
	a.Router.HandleFunc("/login", a.loginHtml).Methods("GET")
	a.Router.HandleFunc("/loginForm", a.loginForm).Methods("POST")
	a.Router.HandleFunc("/registerForm", a.registerForm).Methods("POST")
	a.Router.HandleFunc("/editProfile", a.editForm).Methods("POST")
	a.Router.HandleFunc("/register", a.register).Methods("GET")
	a.Router.HandleFunc("/updateForm", a.updateForm).Methods("POST")
	a.Router.HandleFunc("/home", a.home).Methods("POST")
	a.Router.HandleFunc("/logout", a.logout).Methods("GET")
	a.Router.HandleFunc("/auth/{provider}", a.authGoogle).Methods("GET")
	a.Router.HandleFunc("/auth/{provider}/callback", a.googleCallback).Methods("GET")
	userRouter := a.Router.PathPrefix("/user").Subrouter()
	userRouter.HandleFunc("/signup", a.signup).Methods("POST")
	userRouter.HandleFunc("/login", a.login).Methods("POST")
	userRouter.HandleFunc("/updateUser", middelware.AutherizeJWT(a.updateUser)).Methods("PUT")
	userRouter.HandleFunc("/getUser/{email}", middelware.AutherizeJWT(a.getUser)).Methods("GET")

}
