package main

import (
	"log"
	"os"

	"bitbucket.com/abhinavmishra094/ab_do/app"
	_ "github.com/go-playground/validator/v10"
	_ "github.com/go-sql-driver/mysql"
	_ "github.com/golang-jwt/jwt"
	_ "github.com/gorilla/mux"
	_ "github.com/gorilla/pat"
	_ "github.com/gorilla/sessions"
	"github.com/joho/godotenv"
	_ "github.com/markbates/goth"
	_ "github.com/markbates/goth/gothic"
	_ "github.com/markbates/goth/providers/google"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	a := app.App{}
	a.Initialize(
		os.Getenv("APP_DB_USERNAME"),
		os.Getenv("APP_DB_PASSWORD"),
		os.Getenv("APP_DB_NAME"),
		os.Getenv("APP_DB_HOST"))

	a.Run(os.Getenv("APP_PORT"))
}
